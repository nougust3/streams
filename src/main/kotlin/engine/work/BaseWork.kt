package engine.work

import kotlinx.coroutines.Job

abstract class BaseWork: IWork {
    override lateinit var job: Job

    override fun cancel() {
        job.cancel()
    }
}