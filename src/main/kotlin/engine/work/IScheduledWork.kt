package engine.work

import engine.scheduler.ITimeSpan
import java.util.*

interface IScheduledWork: IWork {

    var date: Date?
    var interval: ITimeSpan?

    fun at(date: Date): IScheduledWork {
        this.date = date
        return this
    }

    fun every(interval: ITimeSpan): IScheduledWork {
        this.interval = interval
        return this
    }

}