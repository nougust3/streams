package engine.work

import engine.report.Status
import kotlinx.coroutines.Job

interface IWork {

    val job: Job

    val name: String

    var status: Status

    suspend fun execute(): Status

    fun cancel()

}