package engine.work

import engine.report.Status
import engine.scheduler.ITimeSpan
import kotlinx.coroutines.Job
import java.util.*

abstract class BaseScheduledWork: IScheduledWork {
    override lateinit var job: Job

    override var date: Date? = null
    override var interval: ITimeSpan? = null
    override var status = Status.IDLE

    override fun cancel() {
        job.cancel()
    }
}