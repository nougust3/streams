package engine.workflow

import engine.report.Status
import engine.work.IWork
import kotlinx.coroutines.Job

class SequentialFlow(override val name: String): BaseFlow() {

    fun add(work: IWork) = works.add(work)

    override val job: Job
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
    override var status: Status
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
        set(value) {}

    override suspend fun execute(): Status {
        status = Status.RUNNING

        works.forEach {
            currentWork = it
            status = it.execute()

            if(status == Status.FAIL)
                return status
        }

        return status
    }

    override fun cancel() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}