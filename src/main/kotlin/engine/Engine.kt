package engine

import kotlinx.coroutines.*

import engine.report.Status
import engine.work.BaseScheduledWork
import engine.work.BaseWork
import engine.work.IScheduledWork
import engine.work.IWork
import java.util.*

class Engine: IEngine {

    private val processes = ArrayList<IWork>()
    private var status = Status.IDLE

    override fun getProcessCount(): Int = processes.size

    override fun getProcesses(): List<IWork> = processes

    override fun getStatus(): Status = status

    override fun execute(work: IWork) {
        processes.add(work)
        updateStatus()

        (work as BaseWork).job = GlobalScope.launch {
            closeWork(work, work.execute())
        }
    }

    override fun execute(work: IScheduledWork) {
        processes.add(work)
        updateStatus()

        if(work.date != null) {
            (work as BaseScheduledWork).job = GlobalScope.launch {
                delay(work.date!!.time - Date().time)
                closeWork(work, work.execute())
            }
        } else {
            (work as BaseScheduledWork).job = GlobalScope.launch {
                while(true) {
                    delay(work.interval!!.mills)
                    work.execute()
                }
            }
        }
    }

    override fun cancel(work: IWork) {
        processes.find { it -> it.name == work.name }!!.cancel()
        closeWork(work, work.status)
    }

    private fun closeWork(work: IWork, status: Status) {
        processes.removeIf { it.name == work.name }
        updateStatus()
    }

    private fun updateStatus() {
        status = when {
            processes.size > 0 -> Status.RUNNING
            else -> Status.IDLE
        }
    }
}