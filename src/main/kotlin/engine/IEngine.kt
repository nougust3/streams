package engine

import engine.report.Status
import engine.work.IScheduledWork
import engine.work.IWork

interface IEngine {

    fun getStatus(): Status

    fun getProcessCount(): Int

    fun getProcesses(): List<IWork>

    fun execute(work: IWork)

    fun execute(work: IScheduledWork)

    fun cancel(work: IWork)

}