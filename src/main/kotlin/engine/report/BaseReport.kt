package engine.report

class BaseReport(private val status: Status): IReport {

    override fun report(): String {
        return status.toString()
    }

}