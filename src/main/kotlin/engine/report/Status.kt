package engine.report

enum class Status {

    RUNNING,

    IDLE,

    DONE,

    FAIL

}