package engine.report

interface IReport {

    fun report(): String

}