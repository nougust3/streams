package engine.scheduler

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*

fun at(date: Date, func: () -> Unit) {
    val waitTime = date.time - Date().time

    GlobalScope.launch {
        delay(waitTime)
        func()
    }
}

fun every(interval: TimeSpan, func: () -> Unit) {
    GlobalScope.launch {
        while (true) {
            delay(interval.mills)
            func()
        }
    }
}


val Int.seconds: TimeSpan get() = TimeSpan(this * 1000L)
val Int.second: TimeSpan get() = this.seconds

val Int.minutes: TimeSpan get() = TimeSpan(this * 60000L)
val Int.minute: TimeSpan get() = this.minutes

val now: Date get() = Date()