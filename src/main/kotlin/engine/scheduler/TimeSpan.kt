package engine.scheduler

import java.util.*

data class TimeSpan(override val mills: Long): ITimeSpan {

    override operator fun plus(over: ITimeSpan): ITimeSpan {
        return TimeSpan(mills + over.mills)
    }

    override operator fun plus(over: Date): Date {
        return Date(mills + over.time)
    }

    override infix fun and(over: ITimeSpan): ITimeSpan {
        return this + over
    }

    override fun from(date: Date): Date {
        return this + date
    }

}