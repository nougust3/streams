package engine.scheduler

import java.util.*

interface ITimeSpan {
    val mills: Long

    fun plus(over: ITimeSpan): ITimeSpan

    fun plus(over: Date): Date

    fun and(over: ITimeSpan): ITimeSpan

    infix fun from(date: Date): Date

}