package engine

import engine.report.Status
import engine.scheduler.*
import engine.work.BaseScheduledWork
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

class ScheduledWorkTest: StringSpec({

    "work mast be scheduled once" {
        val engine = Engine()
        val work = ScheduledWork("ScheduledWork").at(1.seconds from now)

        engine.execute(work)

        runBlocking {
            engine.getProcessCount() shouldBe 1

            delay(1100)
        }

        engine.getProcessCount() shouldBe 0
    }

    "work mast be scheduled cycled" {
        val engine = Engine()
        val work = ScheduledWorkCycled("ScheduledWork").every(1.second)

        engine.execute(work)

        runBlocking {
            delay(3100)
        }

        (work as ScheduledWorkCycled).runCount shouldBe 3
        work.status shouldBe Status.IDLE
    }

})

class ScheduledWork(override val name: String): BaseScheduledWork() {
    override suspend fun execute(): Status {
        status = Status.RUNNING
        println(name)
        return Status.DONE
    }
}

class ScheduledWorkCycled(override val name: String): BaseScheduledWork() {
    var runCount = 0

    override suspend fun execute(): Status {
        status = Status.RUNNING
        runCount += 1
        status = Status.IDLE
        return status
    }
}