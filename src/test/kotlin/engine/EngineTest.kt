package engine

import engine.report.Status
import engine.work.BaseWork
import engine.work.IWork
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

class EngineTest: StringSpec({

    "engine must be running" {
        Engine().getStatus() shouldBe Status.IDLE
    }

    "engine must return active process count" {
        Engine().getProcessCount() shouldBe 0
    }

    "engine must execute process" {
        val engine: IEngine = Engine()
        val work: IWork = PrintWork("PrintWork")

        runBlocking {
            engine.execute(work)
            engine.getProcessCount() shouldBe 1
            engine.getStatus() shouldBe Status.RUNNING
            engine.getProcesses()[0].name shouldBe "PrintWork"

            delay(2000)
            engine.getProcessCount() shouldBe 0
            engine.getStatus() shouldBe Status.IDLE
        }
    }

    "engine must return processes list" {
        val engine: IEngine = Engine()
        val work1: IWork = PrintWork("PrintWork1")
        val work2: IWork = PrintWork("PrintWork2")

        engine.execute(work1)
        engine.execute(work2)

        engine.getProcesses().map { it -> it.name } shouldBe arrayListOf("PrintWork1", "PrintWork2")
    }

    "engine must run processes simultaneously" {
        val engine: IEngine = Engine()
        val work1: IWork = PrintWork("PrintWork1")
        val work2: IWork = PrintWork("PrintWork2")

        runBlocking {
            engine.execute(work1)
            engine.execute(work2)

            engine.getProcessCount() shouldBe 2

            delay(1500)
            engine.getProcessCount() shouldBe 0
        }
    }

    "engine must cancel work" {
        val engine: IEngine = Engine()
        val work: IWork = PrintWork("CanceledWork")

        runBlocking {
            engine.execute(work)
            engine.getProcessCount() shouldBe 1
            engine.cancel(work)
            engine.getProcessCount() shouldBe 0
        }
    }
})

class PrintWork(override val name: String) : BaseWork() {
    override var status: Status = Status.IDLE

    override suspend fun execute(): Status {
        status = Status.RUNNING

        delay(1000)
        println(name)

        return Status.DONE
    }
}